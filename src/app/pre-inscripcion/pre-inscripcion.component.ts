import { Component, OnInit } from '@angular/core';
import { PreInscripcion } from '@app/_models/pre-inscripcion';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { MailService } from '@app/_services/mail.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pre-inscripcion',
  templateUrl: './pre-inscripcion.component.html',
  styleUrls: ['./pre-inscripcion.component.css']
})
export class PreInscripcionComponent implements OnInit {

  v: PreInscripcion;
  form: FormGroup;

  constructor( 
    private formBuilder: FormBuilder,
    private mailService: MailService,
    private spinner: NgxSpinnerService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.v = new PreInscripcion();

    this.form = this.formBuilder.group({
      nombre: new FormControl('', [
          Validators.required,
          Validators.pattern('[a-zA-Z ]*')
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      celular: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]*'),
        Validators.maxLength(10),
        Validators.minLength(10)
      ]),
    });

  }

  onSubmit() {
    this.spinner.show();
    this.mailService.sendPreInscripcion(this.v).subscribe(
      data => {
        this.spinner.hide();
        Swal.fire(
          'Pre Inscripcion',
          data.message,
          'success'
        ).then((result) => {
          this.goLogin();
        });
      },
      error => {
        this.spinner.hide();
        Swal.fire(
          'Pre Inscripcion',
          'No se pudo enviar, intente nuevamente.',
          'error'
        );
      });
  }

  goLogin() {
    this.router.navigate(['/login']);
  }

}
