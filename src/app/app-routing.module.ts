import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { VoluntarioComponent } from './voluntario/voluntario.component';
import { ConvenioComponent } from './convenio/convenio.component';
import { PrintVoluntarioComponent } from './print-voluntario/print-voluntario.component';
import { PreInscripcionComponent } from './pre-inscripcion/pre-inscripcion.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'pre-inscripcion', component: PreInscripcionComponent },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'voluntario/:id', component: VoluntarioComponent, canActivate: [AuthGuard] },
  { path: 'voluntario', component: VoluntarioComponent, canActivate: [AuthGuard] },
  { path: 'convenio', component: ConvenioComponent, canActivate: [AuthGuard] },
  { path: 'print-voluntario', component: PrintVoluntarioComponent, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
