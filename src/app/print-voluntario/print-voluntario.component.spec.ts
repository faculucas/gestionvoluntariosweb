import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintVoluntarioComponent } from './print-voluntario.component';

describe('PrintVoluntarioComponent', () => {
  let component: PrintVoluntarioComponent;
  let fixture: ComponentFixture<PrintVoluntarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintVoluntarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintVoluntarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
