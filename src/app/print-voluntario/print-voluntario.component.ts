import { Component, OnInit, Input } from '@angular/core';
import { Voluntario } from '@app/_models/voluntario';

@Component({
  selector: 'app-print-voluntario',
  templateUrl: './print-voluntario.component.html',
  styleUrls: ['./print-voluntario.component.css']
})
export class PrintVoluntarioComponent implements OnInit {

  @Input() voluntario: Voluntario;

  constructor() { }

  ngOnInit() {
  }

}
