import { Component } from '@angular/core';
import * as fileSaver from 'file-saver';

import { Usuario } from '@app/_models/usuario';
import { AuthenticationService } from '@app/_services/authentication.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Voluntario } from '@app/_models/voluntario';
import { VoluntarioService } from '@app/_services/voluntario.service';
import { Router } from '@angular/router';
import { FileService } from '@app/_services/file.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({ 
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css'] 
})
export class HomeComponent {
    
    busquedaForm: FormGroup;
    
    user: Usuario;

    voluntarios: Voluntario[];
    countIncompletos: number;

    constructor(private formBuilder: FormBuilder,
                private authenticationService: AuthenticationService,
                private voluntarioService: VoluntarioService,
                private router: Router,
                private fileService: FileService,
                private spinner: NgxSpinnerService) 
    { }

    ngOnInit() {
        this.user = this.authenticationService.currentUserValue.usuario;
        this.reloadData();
        this.busquedaForm = this.formBuilder.group({
            nombre: new FormControl('', [
                Validators.required
            ])
        });
    }

    get f() { return this.busquedaForm.controls; }

    reloadData() {
        this.spinner.show();
        this.voluntarioService.countVoluntariosIncompletos().subscribe(data => {
            this.countIncompletos = data;
            this.spinner.hide();
        });
        this.voluntarioService.getVoluntariosIncompletos().subscribe(data => {
            this.voluntarios = data;
            this.spinner.hide();
        });
    }

    getIncompletos() {
        this.spinner.show();
        this.voluntarioService.getVoluntariosIncompletos().subscribe(data => {
            this.voluntarios = data;
            this.spinner.hide();
        });
    }

    getAll() {
        this.spinner.show();
        this.voluntarioService.getAll().subscribe(data => {
            this.voluntarios = data;
            this.spinner.hide();
        });
    }

    onSubmit() {
        this.spinner.show();
        this.voluntarioService.busquedaNombre(this.f.nombre.value).subscribe(data => {
            this.voluntarios = data;
            this.spinner.hide();
        });
    }
   
    delete(id: number) {
        Swal.fire({
            title: 'Estas seguro?',
            text: "Vas a eliminar un voluntario de la base de datos",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ffc107',
            cancelButtonColor: '#6c757d',
            confirmButtonText: 'Si, borrar.'
          }).then((result) => {
            if (result.value) {
                this.voluntarioService.delete(id).subscribe(data => {
                    Swal.fire({
                        confirmButtonColor: '#ffc107',
                        title: 'Eliminado!',
                        text: "El voluntario se a eliminado de la base de datos",
                        icon: 'success',
                    });
                    this.reloadData();
                });
            }
        })
    }

    edit(id: number) {
        this.router.navigate(['voluntario', id]);
    }

    descargarConvenio() {
        this.fileService.downloadConvenio().subscribe(response => {
                let blob:any = new Blob([response], { type: response.type });
                // fileSaver.saveAs(blob, 'Convenio.pdf');
                window.open(URL.createObjectURL(blob)).print();
            }), error => console.log('Error downloading the file'),
            () => console.info('File downloaded successfully');
    }

    downloadConvenio(idVoluntario: number) {
        this.fileService.downloadConvenioVoluntario(idVoluntario).subscribe(response => {
                let blob:any = new Blob([response], { type: response.type });
                fileSaver.saveAs(blob, 'Convenio');
            }), error => console.log('Error downloading the file'),
            () => console.info('File downloaded successfully');
    }

    downloadAnexo(idVoluntario: number) {
        this.fileService.downloadAnexoVoluntario(idVoluntario).subscribe(response => {
                let blob:any = new Blob([response], { type: response.type });
                fileSaver.saveAs(blob, 'Anexo');
            }), error => console.log('Error downloading the file'),
            () => console.info('File downloaded successfully');
    }

}