import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services/authentication.service';
import { JwtResponse } from './_models/jwt-response';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faEdit, faSave, faSearch, faPrint, faTrashAlt, faFileDownload, faPaperPlane, faFileExport, faCalendar } from '@fortawesome/free-solid-svg-icons';
@Component({ selector: 'app-root', templateUrl: 'app.component.html' })
export class AppComponent {
    currentUser: JwtResponse;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private library: FaIconLibrary
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
        library.addIcons(faEdit, faSave, faSearch, faPrint, faTrashAlt, faFileDownload, faPaperPlane, faFileExport, faCalendar);
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}