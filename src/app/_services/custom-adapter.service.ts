import {Injectable} from '@angular/core';
import {
  NgbDateAdapter,
  NgbDateStruct
} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class CustomAdapterService extends NgbDateAdapter<string>{

  readonly DELIMITER = '-';

  fromModel(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return result;
  }

  toModel(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + this.padNumber(date.month) + this.DELIMITER + this.padNumber(date.day);
    }
    return result;
  }

  padNumber(value: number) {
      return `0${value}`.slice(-2);
  }
}
