import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  private SERVER_URL = '//localhost:8080/mail';
  constructor(private httpClient: HttpClient) { }

  sendConvenioByMail(idVoluntario:number): Observable<any>{	
    return this.httpClient.get(`${this.SERVER_URL}/send/convenio/${idVoluntario}`);
  }

  sendPreInscripcion(preInscripcion: Object): Observable<any>{	
    return this.httpClient.post(`${this.SERVER_URL}/send/preinscripcion`,preInscripcion);
  }
  
}
