import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { AreaTrabajo } from '@app/_models/area-trabajo';

@Injectable({ providedIn: 'root' })
export class AreaTrabajoService {
  
  private baseUrl = '//localhost:8080/areaTrabajo';

  constructor(private http: HttpClient) {}

  get(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  create(areaTrabajo: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, areaTrabajo);
  }

  update(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getAll(): Observable<any> {
    return this.http.get<AreaTrabajo[]>(`${this.baseUrl}`);
  }
}