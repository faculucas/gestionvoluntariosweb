import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private SERVER_URL = '//localhost:8080/archivo';
  constructor(private httpClient: HttpClient) { }

  downloadConvenio(): Observable<any>{	
    return this.httpClient.get(`${this.SERVER_URL}/download/convenio`, {
      responseType: "blob"
    });
  }
  
  upload(data) {
   
    let uploadURL = `${this.SERVER_URL}/upload/convenio`;

    return this.httpClient.post<any>(uploadURL, data);

    // return this.httpClient.post<any>(uploadURL, data, {
    //   reportProgress: true,
    //   observe: 'events'
    // }).pipe(map((event) => {

    //   switch (event.type) {

    //     // case HttpEventType.UploadProgress:
    //     //   //const progress = Math.round(100 * event.loaded / event.total);
    //     //   return { status: 'progress', message: progress };

    //     case HttpEventType.Response:
    //       return event.body;
    //     default:
    //       return `Unhandled event: ${event.type}`;
    //   }
    // })
    // );
  }

  uploadConvenioVoluntario(data,idVoluntario:number) {
   
    let uploadURL = `${this.SERVER_URL}/upload/convenio/${idVoluntario}`;

    return this.httpClient.post<any>(uploadURL, data);
  }

  uploadAnexoVoluntario(data,idVoluntario:number) {
   
    let uploadURL = `${this.SERVER_URL}/upload/anexo/${idVoluntario}`;

    return this.httpClient.post<any>(uploadURL, data);
    
  }

  downloadConvenioVoluntario(idVoluntario:number): Observable<any>{	
    return this.httpClient.get(`${this.SERVER_URL}/download/convenio/${idVoluntario}`, {
      responseType: "blob"
    });
  }

  downloadAnexoVoluntario(idVoluntario:number): Observable<any>{	
    return this.httpClient.get(`${this.SERVER_URL}/download/anexo/${idVoluntario}`, {
      responseType: "blob"
    });
  }

}
