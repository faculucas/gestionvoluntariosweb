import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Voluntario } from '@app/_models/voluntario';

@Injectable({ providedIn: 'root' })
export class VoluntarioService {
  
  private baseUrl = '//localhost:8080/voluntario';

  constructor(private http: HttpClient) {}

  get(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  countVoluntariosIncompletos(): Observable<any> {
    return this.http.get<number>(`${this.baseUrl}/incompletos/count`);
  }

  getVoluntariosIncompletos(): Observable<any> {
    return this.http.get<Voluntario[]>(`${this.baseUrl}/incompletos`);
  }

  create(voluntario: Object): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}`, voluntario);
  }

  update(id: number, value: any){
    return this.http.put<any>(`${this.baseUrl}/${id}`, value);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getAll(): Observable<any> {
    return this.http.get<Voluntario[]>(`${this.baseUrl}`);
  }

  busquedaNombre(nombre: string): Observable<any> {
    return this.http.get<Voluntario[]>(`${this.baseUrl}/busqueda/${nombre}`);
  }
}