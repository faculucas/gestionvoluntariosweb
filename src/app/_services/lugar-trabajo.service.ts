import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { LugarTrabajo } from '@app/_models/lugar-trabajo';

@Injectable({ providedIn: 'root' })
export class LugarTrabajoService {
  
  private baseUrl = '//localhost:8080/lugarTrabajo';

  constructor(private http: HttpClient) {}

  get(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  create(lugarTrabajo: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, lugarTrabajo);
  }

  update(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getAll(): Observable<any> {
    return this.http.get<LugarTrabajo[]>(`${this.baseUrl}`);
  }
}