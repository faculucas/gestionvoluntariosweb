import {ErrorMessage} from "ng-bootstrap-form-validation";
 
export const CUSTOM_ERRORS: ErrorMessage[] = [
  {
    error: "required",
    format: requiredFormat
  }, {
    error: "email",
    format: emailFormat
  }, {
    error: "pattern",
    format: patternFormat
  }, {
    error: "maxlength",
    format: maxLengthFormat
  }, {
    error: "minlength",
    format: minLengthFormat
  }, {
    error: "min",
    format: minFormat
  }, {
    error: "max",
    format: maxFormat
  }
];
 
export function requiredFormat(label: string, error: any): string {
  return `${label} es obligatorio.`;
}
 
export function emailFormat(label: string, error: any): string {
  return `${label} inválido.`;
}

export function patternFormat(label: string, error: any): string {
  return `${label} inválido.`;
}

export function maxLengthFormat(label: string, error: any): string {
  return `Como máximo puede ingresar ${error.requiredLength} caracteres.`;
}

export function minLengthFormat(label: string, error: any): string {
  return `Debe ingresar al menos ${error.requiredLength} caracteres.`;
}

export function minFormat(label: string, error: any): string {
  return `Ingrese un valor mayor a ${error.min}.`;
}

export function maxFormat(label: string, error: any): string {
  return `Ingrese un valor menor a ${error.max}.`;
}