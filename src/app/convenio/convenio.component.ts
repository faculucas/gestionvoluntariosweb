
import { FormBuilder } from '@angular/forms';
import { FileService } from '@app/_services/file.service';
import * as fileSaver from 'file-saver';

import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-convenio',
  templateUrl: './convenio.component.html',
  styleUrls: ['./convenio.component.css']
})
export class ConvenioComponent implements OnInit {

  form: FormGroup;
  error: string;
  userId: number = 1;
  uploadResponse = { status: '', message: '', filePath: '' };

  @ViewChild('labelImport', { static: true })
  labelImport: ElementRef;
  
  constructor(private router: Router,
              private formBuilder: FormBuilder, 
              private fileService: FileService,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      convenio: new FormControl('', [
        Validators.required
      ])
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file:File = event.target.files[0];
      this.labelImport.nativeElement.innerText = file.name;
      this.form.get('convenio').setValue(file);
    }
  }

  download() {
    this.fileService.downloadConvenio().subscribe(response => {
			let blob:any = new Blob([response], { type: response.type });
			fileSaver.saveAs(blob, 'Convenio.pdf');
		}), error => console.log('Error downloading the file'),
    () => console.info('File downloaded successfully');
  }
  
  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('convenio').value);
    this.spinner.show();
    this.fileService.upload(formData).subscribe(
      data => {
        this.spinner.hide();
        Swal.fire(
          'Archivo',
          data.message,
          'success'
        ).then((result) => {
          this.goHome();
        });
      });
  }

  goHome() {
    this.router.navigate(['/']);
  }

}
