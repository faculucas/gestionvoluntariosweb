import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Voluntario } from '@app/_models/voluntario';
import { VoluntarioService } from '@app/_services/voluntario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AreaTrabajoService } from '@app/_services/area-trabajo.service';
import { LugarTrabajo } from '@app/_models/lugar-trabajo';
import { LugarTrabajoService } from '@app/_services/lugar-trabajo.service';
import { AreaTrabajo } from '@app/_models/area-trabajo';
import { DiasDisponibles } from '@app/_models/dias-disponibles';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FileService } from '@app/_services/file.service';

import Swal from 'sweetalert2'
import { MailService } from '@app/_services/mail.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CustomAdapterService } from '@app/_services/custom-adapter.service';
import { CustomDateParserFormatterService } from '@app/_services/custom-date-parser-formatter.service';

@Component({
  selector: 'app-voluntario',
  templateUrl: './voluntario.component.html',
  styleUrls: ['./voluntario.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatterService}
  ]
})
export class VoluntarioComponent implements OnInit {

  voluntarioForm: FormGroup;
  idVoluntario: number;
  v: Voluntario;
  areaTrabajoList: AreaTrabajo[];
  lugarTrabajoList: LugarTrabajo[];

  formConvenio: FormGroup;
  formAnexo: FormGroup;

  @ViewChild('labelImportConvenio', { static: false })
  labelImportConvenio: ElementRef;

  @ViewChild('labelImportAnexo', { static: false })
  labelImportAnexo: ElementRef;

  uploadResponse = { status: '', message: '', filePath: '' };

  error: string;

  fechaNacVar:NgbDateStruct;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private voluntarioService: VoluntarioService,
              private areaTrabajoService: AreaTrabajoService,
              private lugarTrabajoService: LugarTrabajoService, 
              private formBuilder: FormBuilder,
              private fileService: FileService,
              private mailService: MailService,
              private spinner: NgxSpinnerService,
              private dateAdapter: CustomAdapterService) { }

  ngOnInit() {

    this.idVoluntario = this.route.snapshot.params['id'];

    this.v = new Voluntario();
    this.v.diasDisponibles = new DiasDisponibles();

    this.areaTrabajoService.getAll().subscribe(data => {
      this.areaTrabajoList = data;
    });

    this.lugarTrabajoService.getAll().subscribe(data => {
      this.lugarTrabajoList = data;
    });

    if(typeof this.idVoluntario !== 'undefined'){
      this.spinner.show();
      this.voluntarioService.get(this.idVoluntario).subscribe(data => { //viene por edit
        this.v = data;
        this.fechaNacVar = this.dateAdapter.fromModel(this.v.fechaNacimiento);
        this.spinner.hide();
      });
    }

    this.formConvenio = this.formBuilder.group({
      convenio: new FormControl('', [
        Validators.required
      ])
    });

    this.formAnexo = this.formBuilder.group({
      anexo: new FormControl('', [
        Validators.required
      ])
    });

    this.voluntarioForm = this.formBuilder.group({
      nombre: new FormControl('', [
          Validators.required,
          Validators.pattern('[a-zA-Z ]*')
      ]),
      fechaNacimiento: new FormControl('', [
        Validators.required
      ]),
      dni: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]*'),
        Validators.maxLength(8),
        Validators.minLength(8)
      ]),
      sexo: new FormControl('', [
        Validators.required
      ]),
      estadoCivil: new FormControl('', [
        Validators.required
      ]),
      nacionalidad: new FormControl('', [
        Validators.required
      ]),
      domicilio: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      celular: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]*'),
        Validators.maxLength(10),
        Validators.minLength(10)
      ]),
      situacionOcupacional: new FormControl('', [
        Validators.required
      ]),
      estudios: new FormControl('', [
        Validators.required
      ]),
      areaTrabajo: new FormControl('', [
        Validators.required
      ]),
      lugarTrabajo: new FormControl('', [
        Validators.required
      ]),
      experiencia: new FormControl('', []),
      lunes: new FormControl('', []),
      martes: new FormControl('', []),
      miercoles: new FormControl('', []),
      jueves: new FormControl('', []),
      viernes: new FormControl('', []),
      sabado: new FormControl('', []),
      domingo: new FormControl('', []),
      feriado: new FormControl('', []),
      desde: new FormControl('', [
        Validators.required,
        Validators.maxLength(2),
        Validators.minLength(0),
        Validators.min(0),
        Validators.max(23),
      ]),
      hasta: new FormControl('', [
        Validators.required,
        Validators.maxLength(2),
        Validators.minLength(0),
        Validators.min(0),
        Validators.max(23),
      ])
      
    });
    
  }

  get f() { return this.voluntarioForm.controls; }

  compareItems(i1, i2) {
    return i1 && i2 && i1.id===i2.id;
  }

  onSubmit() {
    if(typeof this.idVoluntario === 'undefined'){
      this.voluntarioService.create(this.v).subscribe(
        resultado => {
          Swal.fire({
            confirmButtonColor: '#ffc107',
            icon: 'success',
            title: 'Voluntario creado!',
            text: 'Se genero correctamente el registro en la base de datos.',
          }).then((result) => {
            this.goHome();
          })
        },
        error => {
          this.spinner.hide();
          Swal.fire(
            'Ha ocurrido un error',
            error,
            'error'
          );
        }
      );
    }else{
      this.v.fechaNacimiento = this.dateAdapter.toModel(this.fechaNacVar);
      this.voluntarioService.update(this.idVoluntario,this.v).subscribe(
        resultado => {
          Swal.fire({
            confirmButtonColor: '#ffc107',
            icon: 'success',
            title: 'Voluntario editado!',
            text: 'Se edito correctamente el registro en la base de datos.',
          }).then((result) => {
            this.goHome();
          })
        },
        error => {
          this.spinner.hide();
          Swal.fire(
            'Ha ocurrido un error',
            error,
            'error'
          );
        }
      );
    }
  }

  goHome() {
    this.router.navigate(['/']);
  }

  onFileChangeConvenio(event) {
    if (event.target.files.length > 0) {
      const file:File = event.target.files[0];
      this.labelImportConvenio.nativeElement.innerText = file.name;
      this.formConvenio.get('convenio').setValue(file);
      
    }
  }

  onFileChangeAnexo(event) {
    if (event.target.files.length > 0) {
      const file:File = event.target.files[0];
      this.labelImportAnexo.nativeElement.innerText = file.name;
      this.formAnexo.get('anexo').setValue(file);
    }
  }

  uploadConvenio() {
    let formData = new FormData();
    formData.append('file', this.formConvenio.get('convenio').value);
    this.spinner.show();
    this.fileService.uploadConvenioVoluntario(formData,this.idVoluntario).subscribe(
      data => {
        this.spinner.hide();
        Swal.fire(
          'Archivo',
          data.message,
          'success'
        );
      });
  }

  uploadAnexo() {
    let formData = new FormData();
    formData.append('file', this.formAnexo.get('anexo').value);
    this.spinner.show();
    this.fileService.uploadAnexoVoluntario(formData,this.idVoluntario).subscribe(
      data => {
        this.spinner.hide();
        Swal.fire(
          'Archivo',
          data.message,
          'success'
        );
      });
  }

  sendMailConvenio(id: number) {
    this.spinner.show();
    this.mailService.sendConvenioByMail(id).subscribe(
    data => {
      this.spinner.hide();
      Swal.fire(
        'Convenio',
        data.message,
        'success'
      );
    },
    error => {
      this.spinner.hide();
      Swal.fire(
        'Convenio',
        'No se pudo enviar, intente nuevamente.',
        'error'
      );
    });
  }

}
