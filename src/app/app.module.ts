import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPrintModule } from 'ngx-print';
import { NgxSpinnerModule } from "ngx-spinner";
import {
  NgBootstrapFormValidationModule,
  CUSTOM_ERROR_MESSAGES
} from "ng-bootstrap-form-validation";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

import { ErrorInterceptor } from './_helpers/error.interceptor';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { VoluntarioComponent } from './voluntario/voluntario.component';
import { VoluntarioService } from './_services/voluntario.service';
import { AreaTrabajoService } from './_services/area-trabajo.service';
import { LugarTrabajoService } from './_services/lugar-trabajo.service';
import { ConvenioComponent } from './convenio/convenio.component';
import { PrintVoluntarioComponent } from './print-voluntario/print-voluntario.component';
import { PreInscripcionComponent } from './pre-inscripcion/pre-inscripcion.component';
import { CUSTOM_ERRORS } from './_helpers/custom-errors';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    VoluntarioComponent,
    ConvenioComponent,
    PrintVoluntarioComponent,
    PreInscripcionComponent,
  ],
  imports: [
    NgxPrintModule,
    NgxSpinnerModule,
    NgbModule,
    FontAwesomeModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule
  ],
  providers: [
    VoluntarioService,
    AreaTrabajoService,
    LugarTrabajoService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: CUSTOM_ERROR_MESSAGES, useValue: CUSTOM_ERRORS, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
