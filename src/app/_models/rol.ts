export class Rol { 

    public static ADMIN_CODE = 'INIT';

    constructor(public id: number, public codigo: string, public descripcion: string){
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static fromData(data : any) : Rol{
		if(!data) return null; 
		let o : Rol  = new Rol(	
            data.id,
            data.codigo,
            data.descripcion
        );
        
		return o; 

    }
}