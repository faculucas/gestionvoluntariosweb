import { AreaTrabajo } from './area-trabajo';
import { DiasDisponibles } from './dias-disponibles';

export class Voluntario {
    id: number;
    nombre: string;
    dni: string;
    fechaNacimiento: string;
    sexo: string;
    areaTrabajo: AreaTrabajo;
    domicilio: string;
    email: string;
    celular: string;
    experiencia: string;
    desde: number;
    hasta: number;
    estadoCivil: string;
    nacionalidad: string;
    situacionOcupacional: string;
    estudios: string;
    diasDisponibles: DiasDisponibles;
    fechaModificacionConvenio: string;
    fechaModificacionAnexo: string;
    vencido: boolean;
}