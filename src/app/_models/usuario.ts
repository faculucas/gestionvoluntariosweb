import { Rol } from './rol';

export class Usuario {
    
    public id?: number;
    public username: string;
    public nombre?: string;
    public apellido?: string;
    public telefono?: string;
    public roles?: Rol[] = [];
    public activo?: boolean;
    public bloqueado?: boolean;
    public email?: string;
    public esVerificado: boolean = false;
    public profilePic = '';
    public estado = '';
    
    constructor(id: number, nombre: string, apellido: string, email: string, roles: Rol[], estado: string) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.roles = roles;
        this.estado = estado;
    }

    public static fromData(data: any): Usuario {
        if (!data) return null;
        let u: Usuario = new Usuario(data.id, data.nombre, data.apellido, data.email, data.roles, data.estado.codigo);
        u.id = data.id;
        u.username = data.username;
        u.nombre = data.nombre;
        u.apellido = data.apellido;
        u.telefono = data.telefono;
        u.roles = data.roles? data.roles.map(Rol.fromData) :[];
        u.email = data.email;
        u.activo = (data.activo != undefined) ? data.activo : true;
        u.bloqueado = data.bloqueado;
        u.esVerificado = data.verificado;
        u.estado = data.estado.codigo;
        return u;
    }

    esAdmin() {
        return this.roles.some(r => r.codigo == Rol.ADMIN_CODE);
    }
    tieneRol(roles: string[]) {
        for (let rol = 0; rol < roles.length; rol++)
            if (this.roles.filter(r => r.codigo === roles[rol]).length > 0)
                return true;

        return false;
    }
    get nombreCompleto(): string {
        return this.nombre + " " + this.apellido;
    }

}