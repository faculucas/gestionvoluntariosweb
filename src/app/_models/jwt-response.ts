import { Usuario } from './usuario';

export class JwtResponse {
    token?: string;
    usuario: Usuario;
}